﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PackMember : MonoBehaviour
{
    [SerializeField] bool playerInSight;
    [SerializeField] bool rush;
    [SerializeField] float distanceToPlayer;
    [SerializeField] float detectDistance;
    [SerializeField] float walkingStrength;
    [SerializeField] float pursueDistance;
    [SerializeField] float fleeDistance;
    Rigidbody rb;
    GameObject player;
    bool receivedTip;
    NavMeshAgent nma;
    //AnimationControl animus;

    // Start is called before the first frame update
    void Start()
    {
        playerInSight = false;
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>();
        receivedTip = false;
        nma = GetComponent<NavMeshAgent>();
        //animus = GetComponent<AnimationControl>();
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        distanceToPlayer = Vector3.Distance(this.transform.position, player.transform.position);
        if (distanceToPlayer < detectDistance)
        {
            playerInSight = true;
        }
        if (rush)
        {
            MoveToPlayer(dt);
        }
        else if(playerInSight && distanceToPlayer < pursueDistance && distanceToPlayer > fleeDistance)
        {
            //animus.SetAnimation("Velociraptor_Idle");
            nma.isStopped = true;
        }
        else if (playerInSight && distanceToPlayer > pursueDistance)
        {
            MoveToPlayer(dt);
        }
        else if (playerInSight && distanceToPlayer < fleeDistance)
        {
            MoveAwayFromPlayer(dt);
        }
        else if (receivedTip)
        {
            MoveToPlayer(dt);
            receivedTip = false;
        }
    }

    private void MoveAwayFromPlayer(float dt)
    {
        //animus.SetAnimation("Velociraptor_Walk");
        //nma.isStopped = true;
        Vector3 myPosition = transform.position;
        Vector3 playerPosition = player.transform.position;
        Vector3 newHeading = playerPosition - myPosition;
        newHeading = new Vector3(
            -newHeading.x,
            newHeading.y,
            -newHeading.z);
        newHeading = newHeading.normalized * 100.0f;
        nma.SetDestination(myPosition + newHeading);
        nma.isStopped = false;
        //rb.AddForce(newHeading.normalized * walkingStrength * dt);
    }

    private void MoveToPlayer(float dt)
    {
        //animus.SetAnimation("Velociraptor_Walk");
        nma.SetDestination(player.transform.position);
        nma.isStopped = false;
    }

    public void PlayerLocationTip()
    {
        receivedTip = true;
    }

    public void Rush()
    {
        rush = true;
    }

    public bool IsPlayerInSight()
    {
        return playerInSight;
    }
}
