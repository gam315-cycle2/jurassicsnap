﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreScreenScript : MonoBehaviour
{
    private GameObject gameManagerObj;
    private DatabaseManager database;
    private LoadPictureScript myLPS;
    [SerializeField] private GameObject contentSizeFitter;
    [SerializeField] private GameObject imagePrefab;
    private void Start()
    {
        myLPS = GetComponent<LoadPictureScript>();
        gameManagerObj = GameObject.FindWithTag("GameController");
        database = gameManagerObj.GetComponent<DatabaseManager>();
        database.SaveToDatabase();
        LoadPictures();
    }
    private void LoadPictures()
    {
        foreach(DatabaseManager.Picture pic in database.myPictures)
        {
            GameObject temp = Instantiate(imagePrefab);
            temp.transform.parent = contentSizeFitter.transform;
            temp.transform.localPosition = Vector3.zero;
            myLPS.LoadPicture(temp, pic.img);
            Debug.Log(pic.img);
        }
    }
}
